import urllib2
import json
import math
import os
import numpy as np
import ImageDraw
from cStringIO import StringIO
from PIL import Image
from Queue import PriorityQueue
import xml.etree.ElementTree as ET
import base64
import zlib
import struct
import time
import csv
import datetime
import xml.etree.ElementTree as ET
import pdb
from pymongo import MongoClient  # Database connector
from random import shuffle
import pickle
import os
import random

EARTH_RADIUS = 6371000  # Radius in meters of Earth
GOOGLE_CAR_CAMERA_HEIGHT = 3 # ballpark estimate of the number of meters that camera is off the ground
STEVE_API_KEY = "AIzaSyBnxlcMIMTACiXOOwopa8VAoseY_59pq8w"
STEVE_API_STATIC_KEY = "AIzaSyCou-82BbEst0L9WRXkNvpMcAnGH9mDAH8"

def create_single_annot(name, obj,maintree,loc):
    s = """<annotation>
        <filename>""" + name \
                    + """_z2.jpg</filename>
        <source>
                <annotation>Pasadena Aerial View</annotation>
                <database>Pasadena Tree Database</database>
                <image>google</image>
        </source>
        <object>
                <name>tree</name>
                <bndbox>
                        <xmax>"""+str(int(round(obj['x2']/8)))+"""</xmax>
                        <xmin>"""+str(int(round(obj['x1']/8)))+"""</xmin>
                        <ymax>"""+str(int(round(obj['y2']/8)))+"""</ymax>
                        <ymin>"""+str(int(round(obj['y1']/8)))+"""</ymin>
                </bndbox>
                <pose>Unspecified</pose>
                <truncated>0</truncated>
                <difficult>0</difficult>
        </object><segmented>0</segmented>
        <target>"""+str(maintree)+"""</target>
        <location>"""+str(loc[0])+","+str(loc[1])+"""</location>
        <size>
                <depth>3</depth>
                <height>2048</height>
                <width>1024</width>
        </size>
</annotation>"""
    return s

def create_multi_annot(name, obj, maintree,loc):
    o= ""
    s = """<annotation>
        <filename>""" + name \
                    + """_z2.jpg</filename>        
        <size>
                <depth>3</depth>
                <height>2048</height>
                <width>1024</width>
        </size>
        <source>
                <annotation>Pasadena Aerial View</annotation>
                <database>Pasadena Tree Database</database>
                <image>google</image>
        </source>"""
    for i in range(len(obj)):
        o= o + """<object>
                <name>tree</name>
                <bndbox>
                        <xmax>"""+str(int(round(obj[i]['x2']/8)))+"""</xmax>
                        <xmin>"""+str(int(round(obj[i]['x1']/8)))+"""</xmin>
                        <ymax>"""+str(int(round(obj[i]['y2']/8)))+"""</ymax>
                        <ymin>"""+str(int(round(obj[i]['y1']/8)))+"""</ymin>
                </bndbox>
                <pose>Unspecified</pose>
                <truncated>0</truncated>
                <difficult>0</difficult>
        </object>"""
        
    t = """<target>"""+str(maintree)+"""</target>
            <location>"""+str(loc[0])+","+str(loc[1])+"""</location>
</annotation>"""
    return s+o+t

def get_nearest_pano(
    latitude,
    longitude,
    radius=50,
    rank='closest',
    key=None,
    get_depth_map=False,
    get_pano_map=False,
    panos=None,
    method=None,
    return_pid=False,
    ):
    if panos is None:  # Query google for the nearest pano
        url = \
            'https://cbks0.googleapis.com/cbk?output=json&oe=utf-8&it=all&dm=' \
            + str(int(get_depth_map)) + '&pm=' + str(int(get_pano_map)) \
            + '&rank=' + str(rank) + '&ll=' + str(latitude) + ',' \
            + str(longitude) + '&radius=' + str(radius) \
            + '&cb_client=apiv3&v=4&hl=en-US&gl=US'
        if not key is None:
            url = url + '&key=' + key
        pano = requests.get(url).json() 

        if not return_pid:
            return pano
        else:
            if pano:
                return (pano, pano['Location']['panoId'])
            else:
                return ({}, {})
    else:

        # if we have pre-downloaded a set of panos, search though the dictionary of panos for the closest one

        best_d = 10000000
        pano = None
        closest_pid = None
        for pid in outdoor_panos(panos):
            (lat1, lng1) = get_pano_lat_lng(panos[pid], method=method)
            dist = haversine_distance(latitude, longitude, lat1, lng1)
            if dist < best_d:
                pano = panos[pid]
                best_d = dist
                closest_pid = pid
        if not return_pid:
            return pano
        else:
            return (pano, closest_pid)

def get_pano_lat_lng(pano, method=None):
    if 'CalibratedLocation' in pano:
        return (float(pano['CalibratedLocation']['lat']),
                float(pano['CalibratedLocation']['lng']))
    if method is None:
        method = 'original'
    if method == 'normal':
        return (float(pano['Location']['lat']), float(pano['Location'
                ]['lng']))
    elif method == 'original':
        return (float(pano['Location']['original_lat']),
                float(pano['Location']['original_lng']))
    elif method == 'average':
        return ((float(pano['Location']['lat']) + float(pano['Location'
                ]['original_lat'])) / 2.0, (float(pano['Location']['lng'
                ]) + float(pano['Location']['original_lng'])) / 2.0)
        
def world_coordinates_to_streetview_pixel(
    pano,
    lat,
    lng,
    height=0,
    zoom=None,
    object_dims=None,
    method=None,
    ):
    camera_height = GOOGLE_CAR_CAMERA_HEIGHT  # ballpark estimate of the number of meters that camera is off the ground
    max_zoom = int(pano['Location']['zoomLevels'])
    pitch = 0  # float(pano['Projection']['tilt_pitch_deg'])*math.pi/180
    yaw = float((pano['Projection']['calibrated_pano_yaw_deg'
                ] if 'calibrated_pano_yaw_deg' in pano['Projection'
                ] else pano['Projection']['pano_yaw_deg'])) * math.pi \
        / 180
    (lat1, lng1) = get_pano_lat_lng(pano, method=method)

  # Suppose the camera is at x=0, y=0, and the y-axis points north, and the x-axis points east. Compute the position (dx,dy) corresponding to (lat,lng).
  # dy=math.sin(lat-lat1)*EARTH_RADIUS.  dx=math.cos(lat1)*math.sin(lng-lng1)*EARTH_RADIUS.  math.atan2(dx, dy) is then the clockwise angle from north,
  # which sits in the center of the panorama image (if the panorama image is first shifted by yaw degrees, such that north is in the center of the image).

    (dx, dy) = (math.cos(math.radians(lat1))
                * math.sin(math.radians(lng - lng1)),
                math.sin(math.radians(lat - lat1)))
    look_at_angle = math.pi + math.atan2(dx, dy) - yaw
    while look_at_angle > 2 * math.pi:
        look_at_angle = look_at_angle - 2 * math.pi
    while look_at_angle < 0:
        look_at_angle = look_at_angle + 2 * math.pi
    z = math.sqrt(dx * dx + dy * dy) * EARTH_RADIUS

    if zoom is None:  # default to highest resolution image
        zoom = max_zoom
    down = int(math.pow(2, max_zoom - zoom))  # downsample amount
    (image_width, image_height) = (int(pano['Data']['image_width'])
                                   / down, int(pano['Data'
                                   ]['image_height']) / down)

    if object_dims is None:
        x = image_width * look_at_angle / (2 * math.pi)
        y = image_height / 2 - image_height * (math.atan2(height
                - camera_height, z) - pitch) / math.pi
        return (x, y)
    else:
        x1 = image_width * (math.atan2(-object_dims[0] / 2, z)
                            + look_at_angle) / (2 * math.pi)
        x2 = image_width * (math.atan2(object_dims[0] / 2, z)
                            + look_at_angle) / (2 * math.pi)
        y1 = image_height / 2 - image_height * (math.atan2(height
                + object_dims[1] - camera_height, z) + pitch) / math.pi
        y2 = image_height / 2 - image_height * (math.atan2(height
                - camera_height, z) + pitch) / math.pi
        return (x1, y1, x2, y2)

def get_nearest_pano(
    latitude,
    longitude,
    radius=50,
    rank='closest',
    key=None,
    get_depth_map=False,
    get_pano_map=False,
    panos=None,
    method=None,
    return_pid=False,
    ):
    if panos is None:  # Query google for the nearest pano
        url = \
            'https://cbks0.googleapis.com/cbk?output=json&oe=utf-8&it=all&dm=' \
            + str(int(get_depth_map)) + '&pm=' + str(int(get_pano_map)) \
            + '&rank=' + str(rank) + '&ll=' + str(latitude) + ',' \
            + str(longitude) + '&radius=' + str(radius) \
            + '&cb_client=apiv3&v=4&hl=en-US&gl=US'
        if not key is None:
            url = url + '&key=' + key
        pano = requests.get(url).json() 

        if not return_pid:
            return pano
        else:
            if pano:
                return (pano, pano['Location']['panoId'])
            else:
                return ({}, {})
    else:

        # if we have pre-downloaded a set of panos, search though the dictionary of panos for the closest one

        best_d = 10000000
        pano = None
        closest_pid = None
        for pid in outdoor_panos(panos):
            (lat1, lng1) = get_pano_lat_lng(panos[pid], method=method)
            dist = haversine_distance(latitude, longitude, lat1, lng1)
            if dist < best_d:
                pano = panos[pid]
                best_d = dist
                closest_pid = pid
        if not return_pid:
            return pano
        else:
            return (pano, closest_pid)

def get_pano_lat_lng(pano, method=None):
    if 'CalibratedLocation' in pano:
        return (float(pano['CalibratedLocation']['lat']),
                float(pano['CalibratedLocation']['lng']))
    if method is None:
        method = 'original'
    if method == 'normal':
        return (float(pano['Location']['lat']), float(pano['Location'
                ]['lng']))
    elif method == 'original':
        return (float(pano['Location']['original_lat']),
                float(pano['Location']['original_lng']))
    elif method == 'average':
        return ((float(pano['Location']['lat']) + float(pano['Location'
                ]['original_lat'])) / 2.0, (float(pano['Location']['lng'
                ]) + float(pano['Location']['original_lng'])) / 2.0)
        
def world_coordinates_to_streetview_pixel(
    pano,
    lat,
    lng,
    height=0,
    zoom=None,
    object_dims=None,
    method=None,
    ):
    camera_height = GOOGLE_CAR_CAMERA_HEIGHT  # ballpark estimate of the number of meters that camera is off the ground
    max_zoom = int(pano['Location']['zoomLevels'])
    pitch = 0  # float(pano['Projection']['tilt_pitch_deg'])*math.pi/180
    yaw = float((pano['Projection']['calibrated_pano_yaw_deg'
                ] if 'calibrated_pano_yaw_deg' in pano['Projection'
                ] else pano['Projection']['pano_yaw_deg'])) * math.pi \
        / 180
    (lat1, lng1) = get_pano_lat_lng(pano, method=method)

  # Suppose the camera is at x=0, y=0, and the y-axis points north, and the x-axis points east. Compute the position (dx,dy) corresponding to (lat,lng).
  # dy=math.sin(lat-lat1)*EARTH_RADIUS.  dx=math.cos(lat1)*math.sin(lng-lng1)*EARTH_RADIUS.  math.atan2(dx, dy) is then the clockwise angle from north,
  # which sits in the center of the panorama image (if the panorama image is first shifted by yaw degrees, such that north is in the center of the image).

    (dx, dy) = (math.cos(math.radians(lat1))
                * math.sin(math.radians(lng - lng1)),
                math.sin(math.radians(lat - lat1)))
    look_at_angle = math.pi + math.atan2(dx, dy) - yaw
    while look_at_angle > 2 * math.pi:
        look_at_angle = look_at_angle - 2 * math.pi
    while look_at_angle < 0:
        look_at_angle = look_at_angle + 2 * math.pi
    z = math.sqrt(dx * dx + dy * dy) * EARTH_RADIUS

    if zoom is None:  # default to highest resolution image
        zoom = max_zoom
    down = int(math.pow(2, max_zoom - zoom))  # downsample amount
    (image_width, image_height) = (int(pano['Data']['image_width'])
                                   / down, int(pano['Data'
                                   ]['image_height']) / down)

    if object_dims is None:
        x = image_width * look_at_angle / (2 * math.pi)
        y = image_height / 2 - image_height * (math.atan2(height
                - camera_height, z) - pitch) / math.pi
        return (x, y)
    else:
        x1 = image_width * (math.atan2(-object_dims[0] / 2, z)
                            + look_at_angle) / (2 * math.pi)
        x2 = image_width * (math.atan2(object_dims[0] / 2, z)
                            + look_at_angle) / (2 * math.pi)
        y1 = image_height / 2 - image_height * (math.atan2(height
                + object_dims[1] - camera_height, z) + pitch) / math.pi
        y2 = image_height / 2 - image_height * (math.atan2(height
                - camera_height, z) + pitch) / math.pi
        return (x1, y1, x2, y2)

def get_nearest_pano(
    latitude,
    longitude,
    radius=50,
    rank='closest',
    key=None,
    get_depth_map=False,
    get_pano_map=False,
    panos=None,
    method=None,
    return_pid=False,
    ):
    if panos is None:  # Query google for the nearest pano
        url = \
            'https://cbks0.googleapis.com/cbk?output=json&oe=utf-8&it=all&dm=' \
            + str(int(get_depth_map)) + '&pm=' + str(int(get_pano_map)) \
            + '&rank=' + str(rank) + '&ll=' + str(latitude) + ',' \
            + str(longitude) + '&radius=' + str(radius) \
            + '&cb_client=apiv3&v=4&hl=en-US&gl=US'
        if not key is None:
            url = url + '&key=' + key
        pano = requests.get(url).json() 

        if not return_pid:
            return pano
        else:
            if pano:
                return (pano, pano['Location']['panoId'])
            else:
                return ({}, {})
    else:

        # if we have pre-downloaded a set of panos, search though the dictionary of panos for the closest one

        best_d = 10000000
        pano = None
        closest_pid = None
        for pid in outdoor_panos(panos):
            (lat1, lng1) = get_pano_lat_lng(panos[pid], method=method)
            dist = haversine_distance(latitude, longitude, lat1, lng1)
            if dist < best_d:
                pano = panos[pid]
                best_d = dist
                closest_pid = pid
        if not return_pid:
            return pano
        else:
            return (pano, closest_pid)

def get_pano_lat_lng(pano, method=None):
    if 'CalibratedLocation' in pano:
        return (float(pano['CalibratedLocation']['lat']),
                float(pano['CalibratedLocation']['lng']))
    if method is None:
        method = 'original'
    if method == 'normal':
        return (float(pano['Location']['lat']), float(pano['Location'
                ]['lng']))
    elif method == 'original':
        return (float(pano['Location']['original_lat']),
                float(pano['Location']['original_lng']))
    elif method == 'average':
        return ((float(pano['Location']['lat']) + float(pano['Location'
                ]['original_lat'])) / 2.0, (float(pano['Location']['lng'
                ]) + float(pano['Location']['original_lng'])) / 2.0)
        
def world_coordinates_to_streetview_pixel(
    pano,
    lat,
    lng,
    height=0,
    zoom=None,
    object_dims=None,
    method=None,
    ):
    camera_height = GOOGLE_CAR_CAMERA_HEIGHT  # ballpark estimate of the number of meters that camera is off the ground
    max_zoom = int(pano['Location']['zoomLevels'])
    pitch = 0  # float(pano['Projection']['tilt_pitch_deg'])*math.pi/180
    yaw = float((pano['Projection']['calibrated_pano_yaw_deg'
                ] if 'calibrated_pano_yaw_deg' in pano['Projection'
                ] else pano['Projection']['pano_yaw_deg'])) * math.pi \
        / 180
    (lat1, lng1) = get_pano_lat_lng(pano, method=method)

  # Suppose the camera is at x=0, y=0, and the y-axis points north, and the x-axis points east. Compute the position (dx,dy) corresponding to (lat,lng).
  # dy=math.sin(lat-lat1)*EARTH_RADIUS.  dx=math.cos(lat1)*math.sin(lng-lng1)*EARTH_RADIUS.  math.atan2(dx, dy) is then the clockwise angle from north,
  # which sits in the center of the panorama image (if the panorama image is first shifted by yaw degrees, such that north is in the center of the image).

    (dx, dy) = (math.cos(math.radians(lat1))
                * math.sin(math.radians(lng - lng1)),
                math.sin(math.radians(lat - lat1)))
    look_at_angle = math.pi + math.atan2(dx, dy) - yaw
    while look_at_angle > 2 * math.pi:
        look_at_angle = look_at_angle - 2 * math.pi
    while look_at_angle < 0:
        look_at_angle = look_at_angle + 2 * math.pi
    z = math.sqrt(dx * dx + dy * dy) * EARTH_RADIUS

    if zoom is None:  # default to highest resolution image
        zoom = max_zoom
    down = int(math.pow(2, max_zoom - zoom))  # downsample amount
    (image_width, image_height) = (int(pano['Data']['image_width'])
                                   / down, int(pano['Data'
                                   ]['image_height']) / down)

    if object_dims is None:
        x = image_width * look_at_angle / (2 * math.pi)
        y = image_height / 2 - image_height * (math.atan2(height
                - camera_height, z) - pitch) / math.pi
        return (x, y)
    else:
        x1 = image_width * (math.atan2(-object_dims[0] / 2, z)
                            + look_at_angle) / (2 * math.pi)
        x2 = image_width * (math.atan2(object_dims[0] / 2, z)
                            + look_at_angle) / (2 * math.pi)
        y1 = image_height / 2 - image_height * (math.atan2(height
                + object_dims[1] - camera_height, z) + pitch) / math.pi
        y2 = image_height / 2 - image_height * (math.atan2(height
                - camera_height, z) + pitch) / math.pi
        return (x1, y1, x2, y2)

def download_full_pano_image(pano, zoom=None, key=None, api='javascript', out_dir=None, tiles=None, max_retries=0):
  max_zoom = int(pano['Location']['zoomLevels'])
  if zoom is None: zoom = max_zoom  # default to highest resolution image
  down = int(math.pow(2,max_zoom-zoom))  # downsample amount
  image_width, image_height = int(pano['Data']['image_width'])/down, int(pano['Data']['image_height'])/down
  tile_width, tile_height = int(pano['Data']['tile_width']), int(pano['Data']['tile_height'])
  full_pano_image = Image.new("RGB", (image_width, image_height), "black")
  num_x, num_y = int(math.ceil(image_width / float(tile_width))), int(math.ceil(image_height / float(tile_height)))
  if not out_dir is None: 
    full_name = out_dir + '/' + pano['Location']['panoId'] + '_z' + str(zoom) + '.jpg'
    if os.path.isfile(full_name):
      try:
        im = Image.open(full_name)
        return im
      except IOError:
        print "IOError reading image " + full_name + " in download_full_pano_image()"
        os.rename(full_name, full_name + '.bad')
    print 'Downloading ' + full_name
  for tile_y in range(num_y):
    for tile_x in range(num_x):
      download = tiles is None or tiles[tile_x,tile_y]
      tile = None
      if not out_dir is None and download: 
        fname = out_dir + '/' + pano['Location']['panoId'] + '_x' + str(tile_x) + '_y' + str(tile_y) + '_z' + str(zoom) + '.jpg'
        if os.path.isfile(fname):
          try:
            tile = Image.open(fname)
            download = False
          except IOError:
            print "IOError reading image " + fname + " in download_full_pano_image()"
            os.rename(fname, fname + '.bad')
      if download:
        if api=='javascript':  # javascript api
          url = 'http://cbk0.google.com/cbk?output=tile&panoid='+pano['Location']['panoId']+'&zoom='+str(zoom)+'&x='+str(tile_x)+'&y='+str(tile_y)
        else:  # static street view api, doesn't work currently, not implemented correctly
          url = 'https://maps.googleapis.com/maps/api/streetview?size='+pano['Data']['tile_width']+'x'+pano['Data']['tile_height']+'&pano='+pano['Location']['panoId']+'&fov='+str(360.0/num_x)+'&heading='+str((tile_x+.5)*360.0/num_x)+'&pitch='+str((num_y/2.0-tile_y+.5)*180.0/num_y)
        if not key is None: url = url + '&key=' + key
        for num_retries in range(1+max_retries):
          try:
            response = urllib2.urlopen(url)
            data = response.read()
          except urllib2.HTTPError, err:
            #print 'Error '+str(err.code)+' downloading image ' + url + ' ' + fname
            print 'Error '+str(err.code)+' downloading pano ' + url + ' ' + str(pano['Location']['panoId'])
            if err.code == 400:
              print 'aborting ' + url + ' ' + fname
              open(fname+'.bad', 'a').close()
              return None
            if num_retries < max_retries:
              time.sleep(math.pow(2, num_retries+1))
              continue
          except:
            print 'Unknown Error downloading image ' + url + ' ' + fname
            if num_retries < max_retries:
              time.sleep(math.pow(2, num_retries+1))
              continue
        try:
          tile = Image.open(StringIO(data))
          if not out_dir is None: tile.save(fname)
        except:
          print "IOError reading image " + fname + " in download_full_pano_image()"
          open(full_name+'.bad', 'a').close()
          return None
      if not tile is None:
        full_pano_image.paste(tile, (tile_x*tile_width, tile_y*tile_height))
  #if not out_dir is None and tiles is None: 
  if not out_dir is None: 
    print 'saving full pano: ', full_name
    full_pano_image.save(full_name)
         
  return full_pano_image

# Download the full 360 degree panorama images for a set of streetview locations (e.g., the result of crawl_all_panos_in_rois()).  To download
# all pasadena panos (after running crawl_all_panos_in_rois()), run:
# out_dir = 'pasadena_panos'
# for roi in PASADENA_ROIS:
#   with open(out_dir+'/'+roi['name']+'.json') as f:
#     panos = json.load(f)
#   download_full_panos(out_dir+'/'+roi['name'], panos, zoom=2)
def download_full_panos(out_dir, panos, zoom=None, key=None, api='javascript', max_retries=0):
  if not os.path.exists(out_dir):
    os.makedirs(out_dir)
  changed = False
  good_panos = {}
  for pid in outdoor_panos(panos):
    img = download_full_pano_image(panos[pid], out_dir=out_dir, zoom=zoom, key=key, api=api, max_retries=max_retries)
    if not img is None:
      good_panos[pid] = panos[pid]
    else:
      changed = True
  return panos if not changed else good_panos

cl = MongoClient('mongodb://nassar:q2564961w.@ds139459.mlab.com:39459/testt')

db = cl.testt
tree_cols = db['pasadena'].find({"annotated":"yes"})
tree_cols = list(tree_cols)
print("Amount of Panos: ", len(tree_cols))
if not os.path.exists("Pasadena"):
    os.mkdir("Pasadena")

if not os.path.exists("Pasadena/Annotations"):
    os.mkdir("Pasadena/Annotations")

if not os.path.exists("Pasadena/Images"):
    os.mkdir("Pasadena/Images")

file_list=[]
counterpano = 0
for y in range(len(tree_cols)):
    tree_col = tree_cols[y]
    # print("-----------------------------------------------------------------------------------")
    # print("Counter Tree: ", y)
    panos = tree_col['panos']
    if len(panos) == 4:
        loc = [tree_col["location"]["coordinates"][1],tree_col["location"]["coordinates"][0]]
        annotations = []

        for i in range(len(panos)):
            pano = db['pasadena_panos'].find({"_id":panos[i]})
            try:
                pano = list(pano)[0]
                maintree = tree_col['boxesids'][i]
                annotation = ""
                if len(pano['boxes']) == 1:
                    # if not os.path.exists("Pasadena/Annotations/"+pano['_id']+"_z2.xml") and os.path.exists("Pasadena/Images/"+pano['_id']+"_z2.jpg"): 
                    annotation = create_single_annot(pano['_id'],pano['boxes'],maintree,loc)
                    annoter = open("Pasadena/Annotations/"+pano['_id']+"_z2.xml", "w")
                    annoter.write(annotation)
                    annoter.close()
                    counterpano = counterpano + 1
                    # print("Pano Counter: ", counterpano)
                    if os.path.exists("Pasadena/Images/"+pano['_id']+"_z2"+".jpg"):
                        pass
                    else:
                        img = download_full_pano_image(pano['pano'], zoom=2)
                        img.save("Pasadena/Images/"+pano['_id']+"_z2"+".jpg")
                    line = "Pasadena/Images/"+pano['_id']+"_z2.jpg,"+str(int(round(pano['boxes']['x1']/8)))+","+str(int(round(pano['boxes']['y1']/8)))+","+str(int(round(pano['boxes']['x2']/8)))+","+str(int(round(pano['boxes']['y2']/8)))+",tree"
                    # print(line)
                    file_list.append(pano['_id'])
                else:
                    # if not os.path.exists("Pasadena/Annotations/"+pano['_id']+"_z2.xml") and os.path.exists("Pasadena/Images/"+pano['_id']+"_z2.jpg"): 
                    annotation = create_multi_annot(pano['_id'],pano['boxes'],maintree,loc)
                    annoter = open("Pasadena/Annotations/"+pano['_id']+"_z2.xml", "w")
                    annoter.write(annotation)
                    annoter.close()
                    counterpano = counterpano + 1
                    # print("Pano Counter: ", counterpano)
                    if os.path.exists("Pasadena/Images/"+pano['_id']+"_z2"+".jpg"):
                        pass
                    else:
                        img = download_full_pano_image(pano['pano'], zoom=2)
                        img.save("Pasadena/Images/"+pano['_id']+"_z2"+".jpg")
                        
                    for v in range(len(pano['boxes'])):
                        line = "Pasadena/Images/"+pano['_id']+"_z2.jpg,"+str(int(round(pano['boxes'][v]['x1']/8)))+","+str(int(round(pano['boxes'][v]['y1']/8)))+","+str(int(round(pano['boxes'][v]['x2']/8)))+","+str(int(round(pano['boxes'][v]['y2']/8)))+",tree"
                        # print(line)
                        file_list.append(pano['_id'])
            except:
                print(list(pano))
                print("------------------------######### SKIP ###########---------------------------")
print("Pano Counter: ", counterpano)

print("------------------------########   DUMPED  ########---------------------------")


# print("Files Count: ", file_list)

with open('Pasadena/output.obj', 'wb') as fp:
     pickle.dump(file_list, fp)

# shuffle(file_list)

starter = int(0.6*len(file_list))

mid = int(0.15*len(file_list))
end = int(0.1*len(file_list))

with open('train.txt', 'w') as f:
    for x in range(0, starter):
        f.write("%s\n" % file_list[x])

with open('val.txt', 'w') as f:
    for item in range(starter,starter+mid):
        f.write("%s\n" % file_list[item])

with open('test.txt', 'w') as f:
    for item in range(starter+mid,starter+mid+mid):
        f.write("%s\n" % file_list[item])

with open('trainval.txt', 'w') as f:
    for item in range(starter+mid+mid,starter+mid+mid+end):
        f.write("%s\n" % file_list[item])
